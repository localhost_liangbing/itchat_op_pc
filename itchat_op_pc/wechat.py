import itchat
import os
import time
import cv2
from PIL import ImageGrab
import myEmail

sendMsg = u"{消息助手}：暂时无法回复"
usageMsg = "命令格式:\n"
usageMsg += "调用摄像头拍照：cap vid\n"
usageMsg += "截取屏幕：cap win\n"
usageMsg += "列出目录文件：file fist#桌面\n"
usageMsg += "获取文件：file get#桌面#1\n"
usageMsg += "发送文件到eamil：file email#桌面#1\n"


#打开标志位会存储收到的消息到message.txt
flag = 0 
filename = "message.txt"
myfile = open(filename, 'a+')

#文件列表
file_dist = {}
#遍历的key
file_path = {"桌面":os.path.join(os.path.expanduser("~"), 'Desktop'),"C":"C:/"}

@itchat.msg_register('Text')
def text_reply(msg):
    global flag
    message = msg['Text']
    fromName = msg['FromUserName']
    toName = msg['ToUserName']

    if toName == "filehelper":
        if message == "help":
            itchat.send(usageMsg, "filehelper")
        elif message == "cap vid":
            cap = cv2.VideoCapture(0)
            ret, img = cap.read()
            cv2.imwrite("capVid.png", img)
            itchat.send('@img@%s'%u'capVid.png', 'filehelper')
            cap.release()
        elif message == "cap win":
            ImageGrab.grab().save("capWin.png")
            itchat.send('@img@%s'%u'capWin.png', 'filehelper')
        elif message.startswith("file list"):
            item = message.split("#")
            #itchat.send("file list", "filehelper")
            result = showFileByPath(file_path[item[1]])
            itchat.send(result, "filehelper")
        elif message.startswith("file get"):
            item = message.split("#")
            result = showFileByPath(file_path[item[1]])
            target_file = os.path.join(file_path[item[1]],file_dist[item[2]])
            itchat.send(target_file, "filehelper")
            print(target_file)
            res = itchat.send_file(u"%s" % target_file, toUserName='filehelper')
            print(res)
        elif message.startswith("file email"):
            #itchat.send("file email", "filehelper")
            email_list = []
            item = message.split("#")
            result = showFileByPath(file_path[item[1]])
            target_file = os.path.join(file_path[item[1]], file_dist[item[2]])
            itchat.send(target_file, "filehelper")
            #print(target_file)
            email_list.append(target_file)
            ret=myEmail.mail(email_list)
            if ret:
                itchat.send("邮件发送成功", "filehelper")
            else:
                itchat.send("邮件发送失败", "filehelper")
    elif flag == 1:
        #itchat.send(sendMsg, fromName)
        myfile.write(message)
        myfile.write("\n")
        myfile.flush()


#加载当前目录的txt文件       
def showFileByPath(path ="."):
    global file_dist
    allfiles = os.listdir(path)
    index = 1
    file_dist = {}
    result = ""
    for file in allfiles:
        if os.path.isfile(path+"/"+file):
            print("\t"+str(index)+":\t"+file) #当前目录路径
            file_dist[str(index)] = file
            result += str(index)+":\t"+file+"\n"
            index = index + 1
    return result
if __name__ == '__main__':
    itchat.auto_login(hotReload=True)
    itchat.send(usageMsg, "filehelper")
    itchat.run()