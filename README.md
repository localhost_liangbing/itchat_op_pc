# itchat_op_pc

#### 介绍
使用itchat微信库，利用文件助手发送命令操作电脑，包括调用视频查看使用者，截屏，发送文件等。

#### 软件架构




- Python 3.6.1 
- itchat
- cv2
- PIL



#### 安装教程

下载后修改config.ini

```
[email]
sender= 发件人邮箱账号
pass = 发件人邮箱密码
addressee = 收件人邮箱账号，可以发送给自己
```


#### 使用说明

1. py -3 wechat.py 

2. 扫码登录

3. 
截屏
![截取屏幕](https://images.gitee.com/uploads/images/2019/0310/230412_7dc62f3b_2226959.jpeg "win.jpg")

摄像头拍照，手机壳保命
![调用摄像头拍照](https://images.gitee.com/uploads/images/2019/0310/230455_c589d4c8_2226959.jpeg "vid.jpg")

文件操作，替换成了当前目录
file_path = {"桌面":'.',"C":"C:/"}
![文件操作](https://images.gitee.com/uploads/images/2019/0310/230529_8a5e75ef_2226959.jpeg "file.jpg")

![邮件](https://images.gitee.com/uploads/images/2019/0310/230542_f9379573_2226959.jpeg "email.jpg")


